const obj = require("../problem2");

obj
  .readTheFile("../lipsum_1.txt")
  .then((data1) => {
    console.log("Read the lipsum file content: ", data1);
    console.log("\n");

    return obj.createNewFileWithUpperCase("../uppercase.txt", data1);
  })
  .then((success) => {
    console.log("File created successfully with upper-case", success);
    console.log("\n");

    return obj.readTheFile("../uppercase.txt");
  })
  .then((data2) => {
    console.log("Read the uppercase file content: ", data2);
    console.log("\n");

    return obj.covertFileInLowerCaseWithSplit("../lowercase.txt", data2);
  })
  .then((data3) => {
    console.log("File created successfully with lower-case and split: ", data3);
    console.log("\n");

    return obj.readTheFile("../lowercase.txt");
  })
  .then((data4) => {
    console.log("Read the lowercase file content: ", data4);
    console.log("\n");

    return obj.sortTheContentOfFile("../sortcontent.txt", data4);
  })
  .then((data5) => {
    console.log("File created successfully with sorting the content: ", data5);
    console.log("\n");

    return obj.readTheFile("../sortcontent.txt");
  })
  .then((data5) => {
    console.log("Read the sort content file content: ", data5);
    console.log("\n");

    return obj.readTheFile("../filenames.txt");
  })
  .then((filesName) => {
    filesName = filesName.split(",");

    return obj.deleteTheFiles(filesName);
  })
  .then(() => {
    console.log("Files deleted successfully.");
  })
  .catch((error) => {
    console.log("Error:", error);
  });
