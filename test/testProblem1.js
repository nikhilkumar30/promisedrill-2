const fs_problem1 = require("../problem1");
const path = require("path");

const dirpath = path.join(__dirname, "test");
const numberOfFiles = 3;

fs_problem1
  .createFiles(dirpath, numberOfFiles)
  .then((createdFiles) => {
    console.log("Files created:", createdFiles);

    return fs_problem1.deleteFiles(createdFiles);
  })
  .then((deleteResult) => {
    console.log(deleteResult);
  })
  .catch((error) => {
    console.error("Error:", error.message);
  });
