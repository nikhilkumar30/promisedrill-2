const path = require("path");
const fs = require("fs").promises;

function createFiles(dirpath, numberOfFiles) {
  return fs
    .mkdir(dirpath)
    .then(() => {
      const createdFiles = [];

      const createFilePromises = Array.from(
        { length: numberOfFiles },
        (_, index) => {
          const fileName = `file${index + 1}.json`;
          const filePath = path.join(dirpath, fileName);
          const randomData = { value: Math.random() };

          return fs
            .writeFile(filePath, JSON.stringify(randomData))
            .then(() => createdFiles.push(filePath));
        }
      );

      return Promise.all(createFilePromises).then(() => createdFiles);
    })
    .catch((err) => {
      throw new Error(`Error creating files: ${err.message}`);
    });
}

function deleteFiles(filePaths) {
  const deleteFilePromises = filePaths.map((filePath) => fs.unlink(filePath));

  return Promise.all(deleteFilePromises)
    .then(() => "Files deleted successfully")
    .catch((err) => {
      throw new Error(`Error deleting files: ${err.message}`);
    });
}

module.exports = { createFiles, deleteFiles };
