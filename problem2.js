const fs = require("fs").promises;

function readTheFile(path) {
  return fs
    .readFile(path, "utf-8")
    .then((data) => data)
    .catch((error) => {
      throw error;
    });
}

function createNewFileWithUpperCase(path, data) {
  return fs
    .writeFile(path, data.toUpperCase())
    .then(() => {
      return fs.appendFile("../filenames.txt", path + ",");
    })
    .then(() => path)
    .catch((error) => {
      throw error;
    });
}

function covertFileInLowerCaseWithSplit(path, content) {
  let data = content.toLowerCase();
  data = data.split("\n");

  return fs
    .writeFile(path, data.join("\n"))
    .then(() => {
      return fs.appendFile("../filenames.txt", path + ",");
    })
    .then(() => path)
    .catch((error) => {
      throw error;
    });
}

function sortTheContentOfFile(path, content) {
  const data = content.split("\n");
  data.sort();

  return fs
    .writeFile(path, data.join("\n"))
    .then(() => {
      return fs.appendFile("../filenames.txt", path + ",");
    })
    .then(() => path)
    .catch((error) => {
      throw error;
    });
}

function deleteTheFiles(files) {
  let numberOfFilesDeleted = 0;

  const deleteFilePromises = files.map((currentFile) => {
    if (currentFile === "") {
      return Promise.resolve();
    }

    return fs.unlink(currentFile).then(() => {
      numberOfFilesDeleted++;
      if (numberOfFilesDeleted === files.length - 1) {
        return fs.unlink("../filenames.txt");
      }
    });
  });

  return Promise.all(deleteFilePromises)
    .then(() => {
      console.log("Files deleted successfully");
    })
    .catch((error) => {
      throw error;
    });
}

module.exports = {
  readTheFile,
  createNewFileWithUpperCase,
  covertFileInLowerCaseWithSplit,
  sortTheContentOfFile,
  deleteTheFiles,
};
